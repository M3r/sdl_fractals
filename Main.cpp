#include <iostream>
#include <SDL.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "Window.h" 
#include "Particles.h"
#include "Swarm.h"

using namespace mer;

int main(int argc, char * argv[])
{
	// random seed generator
	srand(time(NULL));

	Window appWindow;

	// SDL init and check
	if (!appWindow.init()) { std::cout << "Error initialising SDL." << std::endl; }

	Swarm swarm;

	while (true)
	{
		// program lifetime
		int elapsed = SDL_GetTicks();
		
		swarm.updateSwarm(elapsed);

		// set changing colors
		unsigned char red = (unsigned char)((1 + sin(elapsed * 0.0006)) * 128);
		unsigned char green = (unsigned char)((1 + sin(elapsed * 0.0007)) * 128);
		unsigned char blue = (unsigned char)((1 + sin(elapsed * 0.0008)) * 128);
			   
		const Particles* const pParticles = swarm.getParticles();

		// set particles
		for (int i = 0; i < Swarm::NPARTICLES; i++)
		{
			Particles particle = pParticles[i];

			int x = ((particle.m_x + 1) * Window::WIDTH) / 2;
			int y = particle.m_y * Window::WIDTH / 2 + Window::HEIGHT / 2;
			appWindow.setPixel(x, y, red, green, blue);
		}
		
		appWindow.boxBlur();

		// draw
		appWindow.update();

		// check for messages / events
		if (!appWindow.processEvents()) break;
	}

	appWindow.close();

	return 0;
}