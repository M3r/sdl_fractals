#include "Swarm.h"

namespace mer
{

	Swarm::Swarm() : lastTimeTick(0)
	{
		m_pParticles = new Particles[NPARTICLES];
	}


	Swarm::~Swarm()
	{
		delete [] m_pParticles;
	}

	void Swarm::updateSwarm(int elapsed)
	{
		int interval = elapsed - lastTimeTick;

		for (int i = 0; i < Swarm::NPARTICLES; i++)
		{
			m_pParticles[i].updateParticles(interval);
		}

		lastTimeTick = elapsed;
	}

}