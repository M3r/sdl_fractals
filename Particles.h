#pragma once

#include <stdlib.h>
# define M_PI 3.14159265358979323846

namespace mer
{

	struct Particles
	{
		Particles();
		~Particles();

		double m_x;
		double m_y;

		double m_speed;
		double m_direction;

		void updateParticles(int interval);

	private:
		void init();
	};

}

