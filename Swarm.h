#pragma once

#include "Particles.h"

namespace mer
{

	class Swarm
	{
	public:
		const static int NPARTICLES = 10000;

	private:
		Particles* m_pParticles;
		int lastTimeTick;

	public:
		Swarm();
		~Swarm();

		void updateSwarm(int elapsed);

		const Particles const *getParticles() { return m_pParticles; }
	};

}
