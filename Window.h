#pragma once

#include <iostream>
#include <SDL.h>

namespace mer
{

	class Window
	{
	private:
		SDL_Window* m_window;
		SDL_Renderer* m_renderer;
		SDL_Texture* m_texture;
		Uint32* m_buffer1;
		Uint32* m_buffer2;
		Uint32 pixelColor;
	public:
		const static int WIDTH = 1920;
		const static int HEIGHT = 1080;

	public:
		Window();
		~Window();
		bool init();
		void update();
		void setPixel(int x, int y, Uint8 red, Uint8 green, Uint8 blue);
		bool processEvents();
		bool close();
		void clear();
		void boxBlur();
	};

}

